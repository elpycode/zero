'use strict';

// Tars main-module init
// It is a global var
require('./tars/tars');

// Favicon init
var realFavicon = require ('gulp-real-favicon');
var fs = require('fs');
var FAVICON_DATA_FILE = 'faviconData.json';

const gulp = tars.packages.gulp;

// Require system and user's tasks
// You can add your own tasks.
// All your tasks have to be in tars/user-tasks folder
tars.helpers.tarsFsHelper
    .getTasks()
    .forEach(file => require(file)());

// Register links to main tasks without namespace
// Build-dev task. Build dev-version (without watchers)
gulp.task('build-dev', () => gulp.start('main:build-dev'));

// Dev task. Build dev-version with watchers and livereload
gulp.task('dev', () => gulp.start('main:dev'));

// Build task. Build release version
gulp.task('build', () => gulp.start('main:build'));

// Init task. Just start init task
gulp.task('init', () => gulp.start('service:init'));

// Re-init task. Just start re-init task
gulp.task('re-init', () => gulp.start('service:re-init'));

// Update-deps task. Just start update-deps task
gulp.task('update-deps', () => gulp.start('service:update-deps'));

// Default task. Just start build task
gulp.task('default', () => gulp.start('build'));

// Generate favicon task
gulp.task('favicon', function(done) {
    realFavicon.generateFavicon({
        masterPicture: 'markup/static/favicon.png',
        dest: 'markup/static/misc/icons',
        iconsPath: '/',
        design: {
            ios: {
                pictureAspect: 'noChange',
                assets: {
                    ios6AndPriorIcons: false,
                    ios7AndLaterIcons: false,
                    precomposedIcons: false,
                    declareOnlyDefaultIcon: true
                }
            },
            desktopBrowser: {},
            windows: {
                pictureAspect: 'noChange',
                backgroundColor: '#da532c',
                onConflict: 'override',
                assets: {
                    windows80Ie10Tile: false,
                    windows10Ie11EdgeTiles: {
                        small: false,
                        medium: true,
                        big: false,
                        rectangle: false
                    }
                }
            },
            androidChrome: {
                pictureAspect: 'noChange',
                themeColor: '#ffffff',
                manifest: {
                    display: 'standalone',
                    orientation: 'notSet',
                    onConflict: 'override',
                    declared: true
                },
                assets: {
                    legacyIcon: false,
                    lowResolutionIcons: false
                }
            }
        },
        settings: {
            scalingAlgorithm: 'Mitchell',
            errorOnImageTooSmall: false
        },
        markupFile: FAVICON_DATA_FILE
    }, function() {
        done();
    });
});